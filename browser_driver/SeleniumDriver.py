from selenium import webdriver
from selenium.webdriver.common.keys import Keys


class Selenium:
    def __init__(self, browser_name):
        self.browser = browser_name
        # self.configuration = browser_configuration
        # self.driver_location = driver_location
        self.driver = None

    def initialize(self):
        if self.browser == 'Firefox':
            self.driver = webdriver.Firefox()
            return self.driver

        elif self.browser == 'Chrome':
            self.driver = webdriver.Chrome()
            return self.driver
        else:
            print("Unable to Determine the Browser that needed to be used for the Selenium")

    def open_tab_with_url(self, url):
        self.driver.execute_script("window.open('"+url+"');")
