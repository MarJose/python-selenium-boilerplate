import time

from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By


def login(driver, username, username_xpath, password, password_xpath):
    username_element = driver.find_element_by_xpath(username_xpath)
    username_element.send_keys(username)
    username_element.send_keys(Keys.RETURN)
    time.sleep(5)
    password_element = driver.find_element_by_xpath(password_xpath)
    password_element.send_keys(password)
    password_element.send_keys(Keys.RETURN)


def zoho_logout(driver):
    print("Logging out to Zoho ")
    try:
        isAvatarPresent = EC.presence_of_element_located((By.ID, '//*[@id="topdivuserphoto_4503617000000270001"]'))
        WebDriverWait(driver, 35).until(isAvatarPresent)
    except TimeoutException:
        print("Timed out waiting for page to load")
    finally:
        avatar_element = driver.find_element_by_xpath(
            "/html/body/div[14]/div[11]/div[1]/crm-menu/div[2]/div[1]/crm-profile-menu/div/img")
        avatar_element.click()
        time.sleep(1)
        signout_element = driver.find_element_by_xpath(
            "/html/body/lyte-wormhole[1]/lyte-yield/div/div/lyte-yield/lyte-modal-header/div/div[2]/lyte-button[2]/button")
        signout_element.click()
