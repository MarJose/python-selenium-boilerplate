import time
import os
from dotenv import load_dotenv
from browser_driver import SeleniumDriver
from Auth import AuthHanlder
from Utilities import Helper


def main():
    load_dotenv()
    print("Executing all the functions")
    # time.sleep(5)
    # Initial the Selenium Driver
    # PATH = os.getcwd()
    # FULL_PATH = PATH + "/browser_driver/chrome/linux/chromedriver"
    # FULL_PATH = PATH + "/browser_driver/firefox/linux/geckodriver"
    selenium = SeleniumDriver.Selenium("Firefox")
    driver = selenium.initialize()
    driver.get(os.getenv("Url_target"))
    driver.maximize_window()

    # Login
    username_xpath = "/html/body/div[5]/div[3]/div[2]/form/div[2]/div[1]/div/span/input"
    password_xpath = "/html/body/div[5]/div[3]/div[2]/form/div[2]/div[2]/div[2]/input"
    AuthHanlder.login(driver, os.getenv("Username"), username_xpath, os.getenv('Password'), password_xpath)


if __name__ == '__main__':
    main()
