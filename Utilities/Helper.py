from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


def check_if_xpath_now_existing(driver, xpath, timeout=35):
    try:
        element_check = EC.presence_of_element_located((By.XPATH, xpath))
        WebDriverWait(driver, timeout).until(element_check)
    except TimeoutException:
        print("Timed out waiting for page to load")
        return False
    finally:
        return True


def check_if_tag_id_now_existing(driver, tag_id, timeout=35):
    try:
        element_check = EC.presence_of_element_located((By.ID, tag_id))
        WebDriverWait(driver, timeout).until(element_check)
    except TimeoutException:
        print("Timed out waiting for page to load")
        return False
    finally:
        return True
